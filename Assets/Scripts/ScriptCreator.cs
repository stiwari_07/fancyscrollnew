using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using UnityEngine;

namespace Script {
    public class ScriptCreator : MonoBehaviour {
        
        #region Public Field
        public string m_Namespace;
        public string m_ClassName;

        public List < string > m_PublicFields;
        #endregion Public Field
        
        #region MonoBehaviour Callback
        private void Start () {
            Generate ();
        }
        #endregion MonoBehaviour Callback
        
        #region Public Method
        public void Generate () {
            Debug.Log ( $"<color=yellow>Generate Method: {m_Namespace}</color>" );
            if ( !string.IsNullOrEmpty ( m_Namespace ) ) {
                bool flag = GenerateNamespace ();
            }
        }
        #endregion Public Method
        
        #region Private Method
        private bool GenerateNamespace () {
            string [] folders = m_Namespace.Split ( new [] { '.' }, StringSplitOptions.RemoveEmptyEntries );
            StringBuilder sb = new StringBuilder ();
            sb.Append ( Application.dataPath + "/Scripts" );
            foreach ( string folder in folders ) {
                sb.Append ( "/folder" );
                if ( !Directory.Exists ( sb.ToString () ) ) {
                    Directory.CreateDirectory ( folder );
                }
            }
            return false;
        }
        #endregion Private Method
    }
}